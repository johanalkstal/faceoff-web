module.exports = {
	friendlyName: 'Index',

	description: 'Look up the user and show them the start page, or redirect to the log in page.',

	inputs: {},

	exits: {
		success: {
			responseType: 'view',
			viewTemplatePath: 'pages/homepage',
		},
		notFound: {
			description: 'User was not found.',
			responseType: 'notFound',
		},
	},

	fn: async function (inputs, exits) {
		exits.success()
	},
}
