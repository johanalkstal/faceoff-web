module.exports = {
	friendlyName: 'Login',

	description: 'Login user.',

	inputs: {},

	exits: {
		success: {
			responseType: 'view',
			viewTemplatePath: 'pages/login',
		},
	},

	fn: async function (inputs, exits) {
		exits.success()
	},
}
